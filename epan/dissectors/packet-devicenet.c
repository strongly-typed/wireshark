/* packet-devicenet.c
 * Routines for dissection of DeviceNet
 * DeviceNet Home: www.odva.org
 *
 * This dissector includes items from:
 *    CIP Volume 3: DeviceNet Adaptation of CIP, Edition 1.14
 *
 * Michael Mann
 * Erik Ivarsson <eriki@student.chalmers.se>
 * Hans-Jorgen Gunnarsson <hag@hms.se>
 * Copyright 2012
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
#include "config.h"

#include <epan/packet.h>
#include <epan/uat.h>
#include <epan/prefs.h>
#include <epan/expert.h>
#include <epan/address_types.h>
#include <epan/to_str.h>

#include "packet-cip.h"
#include "packet-socketcan.h"

void proto_register_devicenet(void);
void proto_reg_handoff_devicenet(void);

#define DEVICENET_CANID_MASK            CAN_SFF_MASK

#define MESSAGE_EXPLICIT_MAC_ID_MASK    0x3F

#define MESSAGE_GROUP_1_ID              0x3FF
#define MESSAGE_GROUP_1_MSG_MASK        0x3C0
#define MESSAGE_GROUP_1_MAC_ID_MASK     0x03F

#define MESSAGE_GROUP_2_ID              0x5FF
#define MESSAGE_GROUP_2_MSG_MASK        0x007
#define MESSAGE_GROUP_2_MAC_ID_MASK     0x1F8
#define MESSAGE_GROUP_2_FRAG_MASK       0x80
#define MESSAGE_GROUP_2_XID_MASK        0x40

#define MESSAGE_GROUP_3_ID              0x7BF
#define MESSAGE_GROUP_3_MSG_MASK        0x1C0
#define MESSAGE_GROUP_3_MAC_ID_MASK     0x03F
#define MESSAGE_GROUP_3_FRAG_MASK       0x80
#define MESSAGE_GROUP_3_XID_MASK        0x40

#define MESSAGE_GROUP_4_ID              0x7EF
#define MESSAGE_GROUP_4_MSG_MASK        0x03F

static int proto_devicenet = -1;

static int hf_devicenet_can_id = -1;
static int hf_devicenet_src_mac_id = -1;
static int hf_devicenet_grp1_slave_mac_id = -1;
static int hf_devicenet_grp2_slave_mac_id = -1;
static int hf_devicenet_allocators_mac_id = -1;
static int hf_devicenet_data = -1;
static int hf_devicenet_abb_status = -1;
static int hf_devicenet_abb_timestamp = -1;
static int hf_devicenet_grp_msg1_id = -1;
static int hf_devicenet_grp_msg2_id = -1;
static int hf_devicenet_grp_msg2_frag = -1;
static int hf_devicenet_grp_msg2_xid = -1;
static int hf_devicenet_grp_msg2_mac_id = -1;
static int hf_devicenet_grp_msg3_id = -1;
static int hf_devicenet_grp_msg3_frag = -1;
static int hf_devicenet_grp_msg3_xid = -1;
static int hf_devicenet_grp_msg3_dest_mac_id = -1;
static int hf_devicenet_grp_msg4_id = -1;
static int hf_devicenet_rr_bit = -1;
static int hf_devicenet_service_code = -1;
static int hf_devicenet_connection_id = -1;
static int hf_devicenet_open_exp_src_message_id = -1;
static int hf_devicenet_open_exp_dest_message_id = -1;
static int hf_devicenet_open_exp_msg_req_body_format = -1;
static int hf_devicenet_open_exp_msg_actual_body_format = -1;
static int hf_devicenet_open_exp_group_select = -1;
static int hf_devicenet_open_exp_msg_reserved = -1;
static int hf_devicenet_dup_mac_id = -1;
static int hf_devicenet_dup_mac_id_physical_port_number = -1;
static int hf_devicenet_dup_mac_id_serial_number = -1;
static int hf_devicenet_dup_mac_id_vendor = -1;
static int hf_devicenet_group2_only_unconnected_explicit_request_service = -1;
static int hf_devicenet_group2_explicit_request_class_id = -1;
static int hf_devicenet_group2_explicit_request_instance_id = -1;
static int hf_devicenet_group2_explicit_request_allocation_choice = -1;

static int hf_devicenet_explicit_header = -1;
static int hf_devicenet_explicit_header_frag = -1;
static int hf_devicenet_explicit_header_xid = -1;
static int hf_devicenet_explicit_header_slave_mac_id = -1;

static int hf_devicenet_fragmentation_protocol = -1;

static int hf_devicenet_service_field = -1;
static int hf_devicenet_service_field_rr = -1;
static int hf_devicenet_service_field_service_code = -1;

static int hf_devicenet_allocation_choice = -1;

static int hf_devicenet_allocation_choice_explicit_message = -1;
static int hf_devicenet_allocation_choice_acknowledge_suppression = -1;
static int hf_devicenet_allocation_choice_cyclic = -1;
static int hf_devicenet_allocation_choice_change_of_state = -1;
static int hf_devicenet_allocation_choice_reserved3 = -1;
static int hf_devicenet_allocation_choice_bit_strobed = -1;
static int hf_devicenet_allocation_choice_polled = -1;
static int hf_devicenet_allocation_choice_reserved7 = -1;


static int hf_devicenet_comm_fault_rsv = -1;
static int hf_devicenet_comm_fault_match = -1;
static int hf_devicenet_comm_fault_value = -1;
static int hf_devicenet_offline_ownership_reserved = -1;
static int hf_devicenet_offline_ownership_client_mac_id = -1;
static int hf_devicenet_offline_ownership_allocate = -1;
static int hf_devicenet_vendor = -1;
static int hf_devicenet_serial_number = -1;
static int hf_devicenet_class8 = -1;
static int hf_devicenet_class16 = -1;
static int hf_devicenet_instance8 = -1;
static int hf_devicenet_instance16 = -1;
static int hf_devicenet_attribute = -1;
static int hf_devicenet_fragment_type = -1;
static int hf_devicenet_grp2_fragment_type = -1;
static int hf_devicenet_fragment_count = -1;

static gint ett_devicenet = -1;
static gint ett_devicenet_can = -1;
static gint ett_devicenet_contents = -1;
static gint ett_devicenet_explicit_header = -1;
static gint ett_devicenet_fragmentation = -1;
static gint ett_devicenet_service_field = -1;
static gint ett_devicenet_dup_mac_id_fields = -1;
static gint ett_devicenet_allocation_choice = -1;
static gint ett_devicenet_8_8 = -1;
static gint ett_devicenet_8_16 = -1;
static gint ett_devicenet_16_8 = -1;
static gint ett_devicenet_16_16 = -1;

static expert_field ei_devicenet_invalid_service = EI_INIT;
static expert_field ei_devicenet_invalid_can_id = EI_INIT;
static expert_field ei_devicenet_invalid_msg_id = EI_INIT;
static expert_field ei_devicenet_frag_not_supported = EI_INIT;
static expert_field ei_devicenet_dup_mac_id = EI_INIT;

// ODVA Volume 1, Release 2.0, Chapter 4-2.1, Figure 4.3. Explicit Message Header Format
static int * const explicit_header_fields[] = {
    &hf_devicenet_explicit_header_frag,
    &hf_devicenet_explicit_header_xid,
    &hf_devicenet_explicit_header_slave_mac_id,
    NULL
};

static const true_false_string service_field_rr = {
        "Response",
        "Request",
    };

// ODVA Volume 1, Release 2.0, Chapter 4-4.1, Figure 4.15. Format of DeviceNet Fragmentation Protocol
static int * const fragmentation_fields[] = {
    &hf_devicenet_grp2_fragment_type,
    &hf_devicenet_fragment_count,
    NULL
};

// ODVA Volume 1, Release 2.0, Chapter 4-2.2, Figure 4.5. Service Field Format
static int * const service_field_fields[] = {
    &hf_devicenet_service_field_rr,
    &hf_devicenet_service_field_service_code,
    NULL
};

// ODVA Release 2.0, Volume 1, Chapter 4-5, Figure 4.27. Duplicate MAC ID Check Message Data Field Format
static int * const dup_mac_id_fields[] = {
    &hf_devicenet_service_field_rr,
    &hf_devicenet_dup_mac_id_physical_port_number,
    NULL
};

static int * const allocation_choice_fields[] = {
    &hf_devicenet_allocation_choice_explicit_message,
    &hf_devicenet_allocation_choice_polled,
    &hf_devicenet_allocation_choice_bit_strobed,
    &hf_devicenet_allocation_choice_reserved3,
    &hf_devicenet_allocation_choice_change_of_state,
    &hf_devicenet_allocation_choice_cyclic,
    &hf_devicenet_allocation_choice_acknowledge_suppression,
    &hf_devicenet_allocation_choice_reserved7,
    NULL
};

static int devicenet_address_type = -1;

enum node_behavior {
    NODE_BEHAVIOR_8_8   = 0,
    NODE_BEHAVIOR_8_16  = 1,
    NODE_BEHAVIOR_16_8  = 2,
    NODE_BEHAVIOR_16_16 = 3
};

/* UAT entry structure. */
typedef struct {
    guint mac_id;
    enum node_behavior behavior;

} uat_devicenet_record_t;

static uat_devicenet_record_t *uat_devicenet_records = NULL;
static uat_t *devicenet_uat = NULL;
static guint num_devicenet_records_uat = 0;

static gboolean uat_devicenet_record_update_cb(void* r, char** err) {
    uat_devicenet_record_t* rec = (uat_devicenet_record_t *)r;

    if (rec->mac_id > 63) {
        *err = g_strdup("MAC ID must be between 0-63");
        return FALSE;
    }
    return TRUE;
}

UAT_DEC_CB_DEF(uat_devicenet_records, mac_id, uat_devicenet_record_t)
UAT_VS_DEF(uat_devicenet_records, behavior, uat_devicenet_record_t, enum node_behavior, NODE_BEHAVIOR_8_8, "string")

#if 0
static const enum_val_t bodytype_devicenet_protocol_options[] = {
    { "eightovereight", "8/8", 0 },
    { "eightoversixten", "8/16", 1 },
    { "sixteenovereight", "16/8", 2 },
    { "sixteenoversixteen", "16/16", 3 },
    { NULL, NULL, 0 }
};
#endif

#define SC_OPEN_EXPLICIT_MESSAGE    0x4B
#define SC_CLOSE_EXPLICIT_MESSAGE   0x4C
#define SC_DEVICE_HEARTBEAT_MESSAGE 0x4D
#define SC_DEVICE_SHUTOWN_MESSAGE   0x4E

static const value_string devicenet_service_code_vals[] = {
    GENERIC_SC_LIST

    { SC_OPEN_EXPLICIT_MESSAGE,     "Open Explicit Message Connection Request" },
    { SC_CLOSE_EXPLICIT_MESSAGE,    "Close Connection Request" },
    { SC_DEVICE_HEARTBEAT_MESSAGE,  "Device Heartbeat Message" },
    { SC_DEVICE_SHUTOWN_MESSAGE,    "Device Shutdown Message" },
    { 0, NULL }
};


// ODVA Volume 1, 5-5.4.2 DeviceNet Object Instance Service: Object Class Specific Service, Table 5.32.
#define SC_GRP2_ALLOCATE_MASTER_SLAVE_CONNECTION_SET    0x4B
#define SC_GRP2_RELEASE_GROUP2_IDENTIFIER_SET           0x4C

static const value_string devicenet_grp2_service_code_vals[] =  {
    GENERIC_SC_LIST

    { SC_GRP2_ALLOCATE_MASTER_SLAVE_CONNECTION_SET, "Allocate Master/Slave Connection Set" },
    { SC_GRP2_RELEASE_GROUP2_IDENTIFIER_SET,        "Release Group 2 Identifier Set" },
    { 0, NULL }
};


#define GRP1_MSG_SLAVE_IO_MULTICAST_POLL_RESPONSE                   0x0300
#define GRP1_MSG_SLAVE_IO_CHANGE_OF_STATE_OR_CYCLIC_MESSAGE         0x0340
#define GRP1_MSG_SLAVE_IO_BIT_STROBE_RESPONE_MESAAGE                0x0380
#define GRP1_MSG_SLAVE_IO_POLL_RESPONSE_OR_COS_CYCLIC_ACK_MESSAGE   0x03C0

static const value_string devicenet_grp_msg1_vals[] = {
    { GRP1_MSG_SLAVE_IO_MULTICAST_POLL_RESPONSE, "Slave's I/O Multicast Poll Response" },
    { GRP1_MSG_SLAVE_IO_CHANGE_OF_STATE_OR_CYCLIC_MESSAGE, "Slave's I/O Change of State or Cyclic Message" },
    { GRP1_MSG_SLAVE_IO_BIT_STROBE_RESPONE_MESAAGE, "Slave's I/O Bit-Strobe Response Message" },
    { GRP1_MSG_SLAVE_IO_POLL_RESPONSE_OR_COS_CYCLIC_ACK_MESSAGE, "Slave's I/O Poll Response or COS/Cyclic Ack Message" },
    { 0, NULL }
};

#define GRP2_MSG_MASTER_IO_BIT_STROBE_REQUEST       0x00
#define GRP2_MSG_MASTER_IO_MULTICAST_POLL_GROUP_ID  0x01
#define GRP2_MSG_MASTER_CHANGE_OF_STATE_CYCLIC_ACK  0x02
#define GRP2_MSG_SLAVE_EXPLICT_RESPONSE             0x03
#define GRP2_MSG_MASTER_EXPLICIT_REQUEST            0x04
#define GRP2_MSG_MASTER_IO_POLL_REQUEST             0x05
#define GRP2_MSG_UNCONNECTED_PORT                   0x06
#define GRP2_MSG_DUPLICATE_MAC_ID_CHECK             0x07

static const value_string devicenet_grp_msg2_vals[] = {
    { GRP2_MSG_MASTER_IO_BIT_STROBE_REQUEST,        "Master's I/O Bit-Strobe Command" },
    { GRP2_MSG_MASTER_IO_MULTICAST_POLL_GROUP_ID,   "Master's I/O Multicast Poll Group ID" },
    { GRP2_MSG_MASTER_CHANGE_OF_STATE_CYCLIC_ACK,   "Master's Change of State or Cyclic Acknowledge" },
    { GRP2_MSG_SLAVE_EXPLICT_RESPONSE,              "Slave's Explicit/Unconnected Response" },
    { GRP2_MSG_MASTER_EXPLICIT_REQUEST,             "Master's Explicit Request" },
    { GRP2_MSG_MASTER_IO_POLL_REQUEST,              "Master's I/O Poll Command/COS/Cyclic" },
    { GRP2_MSG_UNCONNECTED_PORT,                    "Group 2 Only Unconnected Explicit Request" },
    { GRP2_MSG_DUPLICATE_MAC_ID_CHECK,              "Duplicate MAC ID Check" },
    { 0, NULL }
};

static const value_string devicenet_grp_msg3_vals[] = {
    { 0x000, "Group 3 Message" },
    { 0x040, "Group 3 Message" },
    { 0x080, "Group 3 Message" },
    { 0x0C0, "Group 3 Message" },
    { 0x100, "Group 3 Message" },
    { 0x140, "Unconnected Explicit Response Message" },
    { 0x180, "Unconnected Explicit Request Message" },
    { 0x1C0, "Invalid Group 3 Message" },
    { 0, NULL }
};

#define GRP4_COMM_FAULT_RESPONSE    0x2C
#define GRP4_COMM_FAULT_REQUEST     0x2D
#define GRP4_OFFLINE_OWNER_RESPONSE 0x2E
#define GRP4_OFFLINE_OWNER_REQUEST  0x2F

static const value_string devicenet_grp_msg4_vals[] = {
    { GRP4_COMM_FAULT_RESPONSE, "Communication Faulted Response Message" },
    { GRP4_COMM_FAULT_REQUEST, "Communication Faulted Request Message" },
    { GRP4_OFFLINE_OWNER_RESPONSE, "Offline Ownership Response Message" },
    { GRP4_OFFLINE_OWNER_REQUEST, "Offline Ownership Request Message" },
    { 0, NULL }
};

static const value_string devicenet_message_body_format_vals[] = {
    { 0x00, "DeviceNet 8/8. Class ID = 8 bit integer, Instance ID = 8 bit integer" },
    { 0x01, "DeviceNet 8/16. Class ID = 8 bit integer, Instance ID = 16 bit integer" },
    { 0x02, "DeviceNet 16/16. Class ID = 16 bit integer. Instance ID = 16 bit integer" },
    { 0x03, "DeviceNet 16/8. Class ID = 16 bit integer. Instance ID = 8 bit integer" },
    { 0x04, "CIP Path. The addressing size is variable and is provided as a Packed EPATH on each request" },
    { 0x05, "Reserved by DeviceNet" },
    { 0x06, "Reserved by DeviceNet" },
    { 0x07, "Reserved by DeviceNet" },
    { 0x08, "Reserved by DeviceNet" },
    { 0x09, "Reserved by DeviceNet" },
    { 0x0A, "Reserved by DeviceNet" },
    { 0x0B, "Reserved by DeviceNet" },
    { 0x0C, "Reserved by DeviceNet" },
    { 0x0D, "Reserved by DeviceNet" },
    { 0x0E, "Reserved by DeviceNet" },
    { 0x0F, "Reserved by DeviceNet" },
    { 0, NULL }
};

static const value_string devicenet_group_select_vals[] = {
    { 0x00, "Message Group 1" },
    { 0x01, "Message Group 2" },
    { 0x02, "Reserved" },
    { 0x03, "Message Group 3" },
    { 0x04, "Reserved by DeviceNet" },
    { 0x05, "Reserved by DeviceNet" },
    { 0x06, "Reserved by DeviceNet" },
    { 0x07, "Reserved by DeviceNet" },
    { 0x08, "Reserved by DeviceNet" },
    { 0x09, "Reserved by DeviceNet" },
    { 0x0A, "Reserved by DeviceNet" },
    { 0x0B, "Reserved by DeviceNet" },
    { 0x0C, "Reserved by DeviceNet" },
    { 0x0D, "Reserved by DeviceNet" },
    { 0x0E, "Reserved by DeviceNet" },
    { 0x0F, "Reserved by Node Ping" },
    { 0, NULL }
};

static const value_string devicenet_fragmented_message_type_vals[] = {
    { 0,    "First Fragment" },
    { 0x40, "Middle fragment" },
    { 0x80, "Last fragment" },
    { 0xC0, "Fragment Acknowledge" },
    { 0, NULL }
};

// ODVA Release 2.0, Volume 1, Chapter 4-4.1, Table 4.9. Fragement Type Bit Values
static const value_string devicenet_grp2_fragmented_message_type_vals[] = {
    { 0, "First Fragment" },
    { 1, "Middle fragment" },
    { 2, "Last fragment" },
    { 3, "Fragment Acknowledge" },
    { 0, NULL }
};


#if 0
static const value_string devicenet_io_attribute_vals[] = {
    {0x01, "Vendor ID"},
    {0x02, "Device Type"},
    {0x03, "Product Code"},
    {0x04, "Revision"},
    {0x05, "Status"},
    {0x06, "Serial Number"},
    {0x07, "Product Name"},
    { 0, NULL }
};
#endif

static gint body_type_8_over_8_dissection(guint8 data_length, proto_tree *devicenet_tree,
                                          tvbuff_t *tvb, packet_info *pinfo _U_, gint offset)
{
    guint16 class_id, instance, attribute;
    attribute_info_t* att_info;
    gint start_offset = offset, length;
    proto_item* ti;

    devicenet_tree = proto_tree_add_subtree(devicenet_tree, tvb, offset, -1, ett_devicenet_8_8, NULL, "DeviceNet 8/8");

    proto_tree_add_item(devicenet_tree, hf_devicenet_class8,  tvb, offset, 1, ENC_LITTLE_ENDIAN);
    class_id = tvb_get_guint8(tvb, offset);
    offset++;

    proto_tree_add_item(devicenet_tree, hf_devicenet_instance8,  tvb, offset, 1, ENC_LITTLE_ENDIAN);
    instance = tvb_get_guint8(tvb, offset);
    offset++;

    if (data_length >= 3)
    {
        attribute = tvb_get_guint8(tvb, offset);
        ti = proto_tree_add_item(devicenet_tree, hf_devicenet_attribute,  tvb, offset, 1, ENC_LITTLE_ENDIAN);
        att_info = cip_get_attribute(class_id, instance, attribute);

        if (att_info != NULL)
            proto_item_append_text(ti, " (%s)", att_info->text);

        offset++;
    }

    length = data_length - (offset-start_offset);
    if (length > 0) {
        proto_tree_add_item(devicenet_tree, hf_devicenet_data, tvb, offset, length, ENC_NA);
        offset += length;
    }
    return offset;
}

static gint body_type_8_over_16_dissection(guint8 data_length, proto_tree *devicenet_tree,
                                           tvbuff_t *tvb, packet_info *pinfo _U_, gint offset)
{
    guint16 class_id, instance, attribute;
    attribute_info_t* att_info;
    proto_item* ti;

    devicenet_tree = proto_tree_add_subtree(devicenet_tree, tvb, offset, -1, ett_devicenet_8_16, NULL, "DeviceNet 8/16");

    proto_tree_add_item(devicenet_tree, hf_devicenet_class8, tvb, offset, 1, ENC_LITTLE_ENDIAN);
    class_id = tvb_get_guint8(tvb, offset);
    offset++;

    proto_tree_add_item(devicenet_tree, hf_devicenet_instance16, tvb, offset, 2, ENC_LITTLE_ENDIAN);
    instance = tvb_get_letohs(tvb, offset);

    if (data_length > 4)
    {
        attribute = tvb_get_guint8(tvb, offset);
        ti = proto_tree_add_item(devicenet_tree, hf_devicenet_attribute,  tvb, offset, 1, ENC_LITTLE_ENDIAN);
        att_info = cip_get_attribute(class_id, instance, attribute);

        if (att_info != NULL)
            proto_item_append_text(ti, " (%s)", att_info->text);

        offset++;
    }

    return offset;
}

static gint body_type_16_over_8_dissection(guint8 data_length, proto_tree *devicenet_tree, tvbuff_t *tvb,
                                           packet_info *pinfo _U_, gint offset)
{
    guint16 class_id, instance, attribute;
    attribute_info_t* att_info;
    proto_item* ti;

    devicenet_tree = proto_tree_add_subtree(devicenet_tree, tvb, offset, -1, ett_devicenet_16_8, NULL, "DeviceNet 16/8");

    proto_tree_add_item(devicenet_tree, hf_devicenet_class16, tvb, offset, 2, ENC_LITTLE_ENDIAN);
    class_id = tvb_get_letohs(tvb, offset);
    offset += 2;

    proto_tree_add_item(devicenet_tree, hf_devicenet_instance8, tvb, offset, 1, ENC_LITTLE_ENDIAN);
    instance = tvb_get_guint8(tvb, offset);
    offset++;

    if (data_length > 4)
    {
        attribute = tvb_get_guint8(tvb, offset);
        ti = proto_tree_add_item(devicenet_tree, hf_devicenet_attribute,  tvb, offset, 1, ENC_LITTLE_ENDIAN);
        att_info = cip_get_attribute(class_id, instance, attribute);

        if (att_info != NULL)
            proto_item_append_text(ti, " (%s)", att_info->text);

        offset++;
    }

    return offset;
}

static gint body_type_16_over_16_dissection(guint8 data_length, proto_tree *devicenet_tree, tvbuff_t *tvb,
                                            packet_info *pinfo _U_, gint offset)
{
    guint16 class_id, instance, attribute;
    attribute_info_t* att_info;
    proto_item* ti;

    devicenet_tree = proto_tree_add_subtree(devicenet_tree, tvb, offset, 4, ett_devicenet_16_16, NULL, "DeviceNet 16/16");

    proto_tree_add_item(devicenet_tree, hf_devicenet_class16, tvb, offset, 2, ENC_LITTLE_ENDIAN);
    class_id = tvb_get_letohs(tvb, offset);
    offset += 2;

    proto_tree_add_item(devicenet_tree, hf_devicenet_instance16, tvb, offset, 2, ENC_LITTLE_ENDIAN);
    instance = tvb_get_letohs(tvb, offset);
    offset+=2;

    if (data_length > 5)
    {
        attribute = tvb_get_guint8(tvb, offset);
        ti = proto_tree_add_item(devicenet_tree, hf_devicenet_attribute,  tvb, offset, 1, ENC_LITTLE_ENDIAN);
        att_info = cip_get_attribute(class_id, instance, attribute);

        if (att_info != NULL)
            proto_item_append_text(ti, " (%s)", att_info->text);

        offset++;
    }

    return offset;
}

static int dissect_devicenet(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree, void* data)
{
    proto_item *ti, *can_id_item,
               *msg_id_item, *service_item;
    proto_tree *devicenet_tree, *can_tree, *content_tree;

    gint offset = 0;
    guint16 message_id;
    guint32 data_length = tvb_reported_length(tvb);
    guint8 source_mac;
    struct can_info can_info;
    guint8 service_rr;
    guint8 *src_address, *dest_address;
    guint8 *slave_address, *header_address;

    DISSECTOR_ASSERT(data);
    can_info = *((struct can_info*)data);

    if (can_info.id & (CAN_ERR_FLAG | CAN_RTR_FLAG | CAN_EFF_FLAG))
    {
        /* Error, RTR and frames with extended ids are not for us. */
        return 0;
    }

    col_set_str(pinfo->cinfo, COL_PROTOCOL, "DeviceNet");

    ti = proto_tree_add_item(tree, proto_devicenet, tvb, offset, -1, ENC_NA);
    devicenet_tree = proto_item_add_subtree(ti, ett_devicenet);

    can_tree = proto_tree_add_subtree_format(devicenet_tree, tvb, 0, 0, ett_devicenet_can, NULL, "CAN Identifier: 0x%04x", can_info.id);
    can_id_item = proto_tree_add_uint(can_tree, hf_devicenet_can_id, tvb, 0, 0, can_info.id);
    proto_item_set_generated(can_id_item);

    /*
     * Message group 1
     */
    if ( can_info.id <= MESSAGE_GROUP_1_ID )
    {
        ti = proto_tree_add_uint(can_tree, hf_devicenet_grp_msg1_id, tvb, 0, 0, can_info.id);
        proto_item_set_generated(ti);
        ti = proto_tree_add_uint(can_tree, hf_devicenet_grp1_slave_mac_id, tvb, 0, 0, can_info.id & MESSAGE_GROUP_1_MAC_ID_MASK);
        proto_item_set_generated(ti);

        /* Set source address */
        src_address = (guint8*)wmem_alloc(pinfo->pool, 1);
        *src_address = (guint8)(can_info.id & MESSAGE_GROUP_1_MAC_ID_MASK);
        set_address(&pinfo->src, devicenet_address_type, 1, (const void*)src_address);

        message_id = can_info.id & MESSAGE_GROUP_1_MSG_MASK;
        col_set_str(pinfo->cinfo, COL_INFO, val_to_str_const(message_id, devicenet_grp_msg1_vals, "Other Group 1 Message"));

        switch (message_id) {
        case 0x000:
            proto_tree_add_item(devicenet_tree, hf_devicenet_abb_status, tvb, offset, 2, ENC_LITTLE_ENDIAN);
            guint16 abb_status = tvb_get_guint16(tvb, offset, ENC_LITTLE_ENDIAN);
            offset += 2;

            proto_tree_add_item(devicenet_tree, hf_devicenet_abb_timestamp, tvb, offset, 4, ENC_LITTLE_ENDIAN);
            guint32 abb_timestamp = tvb_get_guint32(tvb, offset, ENC_LITTLE_ENDIAN);
            offset += 4;

            col_add_fstr(pinfo->cinfo, COL_INFO, "Group 1 Message: ABB: %04x %d", abb_status, abb_timestamp);
            break;

        case GRP1_MSG_SLAVE_IO_MULTICAST_POLL_RESPONSE:
        case GRP1_MSG_SLAVE_IO_CHANGE_OF_STATE_OR_CYCLIC_MESSAGE:
        case GRP1_MSG_SLAVE_IO_BIT_STROBE_RESPONE_MESAAGE:
        case GRP1_MSG_SLAVE_IO_POLL_RESPONSE_OR_COS_CYCLIC_ACK_MESSAGE:
        default:
            if (data_length > 0) {
                proto_tree_add_item(devicenet_tree, hf_devicenet_data, tvb, offset, data_length, ENC_NA);
            }
            break;
        }
    }
    /*
     * Message group 2
     */
    else if (can_info.id <= MESSAGE_GROUP_2_ID )
    {
        guint8 byte1;

        ti = proto_tree_add_uint(can_tree, hf_devicenet_grp_msg2_id, tvb, 0, 0, can_info.id);
        proto_item_set_generated(ti);

        /* create display subtree for the protocol */
        message_id = can_info.id & MESSAGE_GROUP_2_MSG_MASK;
        col_set_str(pinfo->cinfo, COL_INFO, val_to_str_const(message_id, devicenet_grp_msg2_vals, "Unknown"));

        ti = proto_tree_add_uint(can_tree, hf_devicenet_grp2_slave_mac_id, tvb, 0, 0, (can_info.id & MESSAGE_GROUP_2_MAC_ID_MASK));
        proto_item_set_generated(ti);

        /* Get slave address */
        slave_address = (guint8*)wmem_alloc(pinfo->pool, 1);
        *slave_address = (guint8)((can_info.id & MESSAGE_GROUP_2_MAC_ID_MASK) >> 3);

        /* Get address from the header. Response/Request determines which is source and which is destination. */
        header_address = (guint8*)wmem_alloc(pinfo->pool, 1);

        /* Dissect Header */
        switch (message_id)
        {
        case GRP2_MSG_SLAVE_EXPLICT_RESPONSE:
        case GRP2_MSG_MASTER_EXPLICIT_REQUEST:
        case GRP2_MSG_UNCONNECTED_PORT:
            // Explicit Messages with Header accordingly to 4-2.1

            content_tree = proto_tree_add_subtree(devicenet_tree, tvb, offset, -1, ett_devicenet_contents, NULL, "Contents");
            proto_tree_add_bitmask(content_tree, tvb, offset, hf_devicenet_explicit_header, 
                            ett_devicenet_explicit_header, explicit_header_fields, ENC_NA);

            byte1 = tvb_get_guint8(tvb, offset);
            *header_address = byte1 & MESSAGE_EXPLICIT_MAC_ID_MASK;

            offset ++;

            if (byte1 & MESSAGE_GROUP_2_FRAG_MASK)
            {
                // ODVA Release 2.0, Volume 1, Chapter 4-4.1, Figure 4.15. Format of DeviceNet Fragementation Protocol
                proto_tree_add_bitmask(content_tree, tvb, offset, hf_devicenet_fragmentation_protocol,
                            ett_devicenet_fragmentation, fragmentation_fields, ENC_NA);
                offset ++;

                col_append_str(pinfo->cinfo, COL_INFO, " - Fragement");
                pinfo->fragmented = TRUE;

                if (message_id == GRP2_MSG_MASTER_EXPLICIT_REQUEST)
                {
                    // In a Request, source is given in the message and destination in the CAN ID.
                    set_address(&pinfo->src, devicenet_address_type, 1, header_address);
                    set_address(&pinfo->dst, devicenet_address_type, 1, slave_address);
                }
                else if (message_id == GRP2_MSG_SLAVE_EXPLICT_RESPONSE)
                {
                    // In a Response, source is in the CAN ID, destination in the Message
                    set_address(&pinfo->src, devicenet_address_type, 1, slave_address);
                    set_address(&pinfo->dst, devicenet_address_type, 1, header_address);
                }
                // ToDo Fragmentation Protocol
                if ((data_length - offset) > 0) {
                    proto_tree_add_item(content_tree, hf_devicenet_data, tvb, offset, -1, ENC_NA);
                }
            }
            else
            {
                // Non-fragmented Explicit Message Body
                proto_tree_add_bitmask(content_tree, tvb, offset, hf_devicenet_service_field,
                            ett_devicenet_service_field, service_field_fields, ENC_NA);
                service_rr = tvb_get_guint8(tvb, offset);
                offset ++;

                proto_item_set_text(content_tree, "Service: %s (%s)", val_to_str_const(service_rr & CIP_SC_MASK, devicenet_grp2_service_code_vals, "Unknown"),
                    service_rr & CIP_SC_RESPONSE_MASK ? "Response" : "Request");

                col_append_fstr(pinfo->cinfo, COL_INFO, " (%s)", val_to_str_const(service_rr & CIP_SC_MASK, devicenet_grp2_service_code_vals, "Unknown"));

                /* Set addresses */
                if (service_rr & CIP_SC_RESPONSE_MASK)
                {
                    // In a Response, source is in the CAN ID, destination in the Message
                    set_address(&pinfo->src, devicenet_address_type, 1, slave_address);
                    set_address(&pinfo->dst, devicenet_address_type, 1, header_address);
                }
                else
                {
                    // In a Request, source is given in the message and destination in the CAN ID.
                    set_address(&pinfo->src, devicenet_address_type, 1, header_address);
                    set_address(&pinfo->dst, devicenet_address_type, 1, slave_address);
                }

                // Service Specific Arguments of Message Body
                if (service_rr & CIP_SC_RESPONSE_MASK)
                {
                    // Explicit Responses
                    switch (service_rr & CIP_SC_MASK)
                    {
                    case SC_GRP2_ALLOCATE_MASTER_SLAVE_CONNECTION_SET:
                        // Response: ODVA Volume 1, Figure 5.20. Success Response to Allocate_Master/Slave_Connection_Set Request
                        proto_tree_add_item(content_tree, hf_devicenet_open_exp_msg_actual_body_format, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                        offset ++;
                        break;

                    case 0x14:
                        // ODVA Release 2.0, Volume 1, Chapter 4-2.6 Error Response Explicit Message
                        // ToDo

                    default:
                        // Display remainings of message
                        if (data_length - offset > 0) {
                            proto_tree_add_item(content_tree, hf_devicenet_data, tvb, offset, -1, ENC_NA);
                        }
                        break;
                    }
                }
                else
                {
                    /* For non-fragmented Explicit Requests, Class ID and Instance ID are always expected */

                    // ODVA Release 2.0, Volume 1, Chapter 4-2.5, Figure 4.11. Non-fragmented Explicit Request Message Body Format

                    switch (service_rr & CIP_SC_MASK)
                    {
                    case SC_GRP2_ALLOCATE_MASTER_SLAVE_CONNECTION_SET:
                        // Request: ODVA Release 2.0, Volume 1, Chapter 5-5.4.2, Figure 5.19. Allocate_Master/Slave_Connection_Set Request Message
                        proto_tree_add_item(content_tree, hf_devicenet_class8, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                        offset ++;

                        proto_tree_add_item(content_tree, hf_devicenet_instance8, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                        offset ++;

                        proto_tree_add_bitmask(content_tree, tvb, offset, hf_devicenet_allocation_choice, 
                            ett_devicenet_allocation_choice, allocation_choice_fields, ENC_NA);
                        offset ++;

                        proto_tree_add_item(content_tree, hf_devicenet_allocators_mac_id, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                        offset ++;
                        break;

                    case SC_SET_ATT_SINGLE:
                    case SC_GET_ATT_SINGLE:
                        // Message Header and Service Field already consumed, thus -2
                        body_type_8_over_8_dissection(data_length-2, content_tree, tvb, pinfo, offset);

                        break;
                    default:
                        if (data_length - offset > 0) {
                            proto_tree_add_item(content_tree, hf_devicenet_data, tvb, offset, -1, ENC_NA);
                        }
                        break;
                    }
                }
            }
            break;

        case GRP2_MSG_MASTER_IO_POLL_REQUEST:
            if (data_length - offset > 0) {
                proto_tree_add_item(devicenet_tree, hf_devicenet_data, tvb, offset, -1, ENC_NA);
            }
            set_address(&pinfo->dst, devicenet_address_type, 1, slave_address);
            // There is no source address in a Master's I/O Poll Request, all data is payload.
            break;

        case GRP2_MSG_DUPLICATE_MAC_ID_CHECK:
            set_address(&pinfo->src, devicenet_address_type, 1, slave_address);
            set_address(&pinfo->dst, devicenet_address_type, 1, slave_address);

            proto_tree_add_bitmask(devicenet_tree, tvb, offset, hf_devicenet_dup_mac_id, ett_devicenet_dup_mac_id_fields, dup_mac_id_fields, ENC_NA);
            service_rr = tvb_get_guint8(tvb, offset);
            if (service_rr & CIP_SC_RESPONSE_MASK)
            {
                col_append_str(pinfo->cinfo, COL_INFO, " - Response");
                expert_add_info_format(pinfo, devicenet_tree, &ei_devicenet_dup_mac_id,
                        "Duplicate MAC ID %d detected", *slave_address);
            }
            else
            {
                col_append_str(pinfo->cinfo, COL_INFO, " - Request");
            }
            offset ++;

            proto_tree_add_item(devicenet_tree, hf_devicenet_dup_mac_id_vendor, tvb, offset, 2, ENC_LITTLE_ENDIAN);
            offset += 2;

            proto_tree_add_item(devicenet_tree, hf_devicenet_dup_mac_id_serial_number, tvb, offset, 4, ENC_LITTLE_ENDIAN);
            break;

        case GRP2_MSG_MASTER_CHANGE_OF_STATE_CYCLIC_ACK:
            // Master acks to slave
            set_address(&pinfo->dst, devicenet_address_type, 1, slave_address);

            if (data_length - offset > 0) {
                proto_tree_add_item(devicenet_tree, hf_devicenet_data, tvb, offset, -1, ENC_NA);
            }
            break;
        case GRP2_MSG_MASTER_IO_BIT_STROBE_REQUEST:
        case GRP2_MSG_MASTER_IO_MULTICAST_POLL_GROUP_ID:
        default:
            if (data_length - offset > 0) {
                proto_tree_add_item(devicenet_tree, hf_devicenet_data, tvb, offset, -1, ENC_NA);
            }
            break;
        }
    }
    /*
     * Message group 3
     */
    else if (can_info.id <= MESSAGE_GROUP_3_ID )
    {
        guint8 byte1;

        msg_id_item = proto_tree_add_uint(can_tree, hf_devicenet_grp_msg3_id, tvb, 0, 0, can_info.id);
        proto_item_set_generated(msg_id_item);
        ti = proto_tree_add_uint(can_tree, hf_devicenet_src_mac_id, tvb, 0, 0, can_info.id & MESSAGE_GROUP_3_MAC_ID_MASK);
        proto_item_set_generated(ti);

        /* Set source address */
        src_address = (guint8*)wmem_alloc(pinfo->pool, 1);
        *src_address = (guint8)(can_info.id & MESSAGE_GROUP_3_MAC_ID_MASK);
        set_address(&pinfo->src, devicenet_address_type, 1, (const void*)src_address);

        message_id = can_info.id & MESSAGE_GROUP_3_MSG_MASK;
        col_set_str(pinfo->cinfo, COL_INFO, val_to_str_const(message_id, devicenet_grp_msg3_vals, "Unknown"));

        proto_tree_add_item(devicenet_tree, hf_devicenet_grp_msg3_frag, tvb, offset, 1, ENC_NA);
        proto_tree_add_item(devicenet_tree, hf_devicenet_grp_msg3_xid, tvb, offset, 1, ENC_NA);
        proto_tree_add_item(devicenet_tree, hf_devicenet_grp_msg3_dest_mac_id, tvb, offset, 1, ENC_LITTLE_ENDIAN);
        byte1 = tvb_get_guint8(tvb, offset);
        source_mac = byte1 & MESSAGE_GROUP_3_MAC_ID_MASK;

        /* Set destination address */
        /* XXX - This may be source address depending on message type.  Need to adjust accordingly) */
        dest_address = (guint8*)wmem_alloc(pinfo->pool, 1);
        *dest_address = (guint8)source_mac;
        set_address(&pinfo->dst, devicenet_address_type, 1, (const void*)dest_address);
        offset++;

        if (byte1 & MESSAGE_GROUP_3_FRAG_MASK)
        {
            col_set_str(pinfo->cinfo, COL_INFO, "Group 3 Message Fragment");
            pinfo->fragmented = TRUE;

            content_tree = proto_tree_add_subtree(devicenet_tree, tvb, offset, -1, ett_devicenet_contents, NULL, "Fragmentation");

            proto_tree_add_item(content_tree, hf_devicenet_fragment_type, tvb, offset, 1, ENC_LITTLE_ENDIAN);
            proto_tree_add_item(content_tree, hf_devicenet_fragment_count, tvb, offset, 1, ENC_LITTLE_ENDIAN);

            /* TODO: Handle fragmentation */
            proto_tree_add_expert(content_tree, pinfo, &ei_devicenet_frag_not_supported, tvb, offset, -1);

            col_set_str(pinfo->cinfo, COL_INFO,
                        val_to_str_const((tvb_get_guint8(tvb, offset) & 0xC0) >> 6,
                                         devicenet_fragmented_message_type_vals,
                                         "Unknown fragmented message type"));
        }
        else
        {
            service_rr = tvb_get_guint8(tvb, offset);

            content_tree = proto_tree_add_subtree_format(devicenet_tree, tvb, offset, -1, ett_devicenet_contents, NULL,
                        "Service: %s (%s)", val_to_str_const(service_rr & CIP_SC_MASK, devicenet_service_code_vals, "Unknown"),
                        service_rr & CIP_SC_RESPONSE_MASK ? "Response" : "Request");

            proto_tree_add_item(content_tree, hf_devicenet_rr_bit, tvb, offset, 1, ENC_LITTLE_ENDIAN);
            service_item = proto_tree_add_item(content_tree, hf_devicenet_service_code, tvb, offset, 1, ENC_LITTLE_ENDIAN);
            offset++;

            col_set_str(pinfo->cinfo, COL_INFO, val_to_str_const(service_rr & CIP_SC_MASK, devicenet_service_code_vals, "Unknown Service Code"));
            if (service_rr & CIP_SC_RESPONSE_MASK)
            {
                col_append_str(pinfo->cinfo, COL_INFO, " - Response");
            }
            else
            {
                col_append_str(pinfo->cinfo, COL_INFO, " - Request");
            }

            switch(message_id)
            {
            case 0x140:
                switch(service_rr & CIP_SC_MASK)
                {
                case SC_OPEN_EXPLICIT_MESSAGE:
                case SC_CLOSE_EXPLICIT_MESSAGE:
                case SC_DEVICE_HEARTBEAT_MESSAGE:
                case SC_DEVICE_SHUTOWN_MESSAGE:
                /* XXX - ERROR RESPONSE? */
                    break;
                default:
                    expert_add_info_format(pinfo, service_item, &ei_devicenet_invalid_service,
                        "Invalid service code (0x%x) for Group 3 Message ID 5", service_rr & CIP_SC_MASK);
                    break;
                }
                break;
            case 0x180:
                switch(service_rr & CIP_SC_MASK)
                {
                case SC_OPEN_EXPLICIT_MESSAGE:
                case SC_CLOSE_EXPLICIT_MESSAGE:
                    break;
                default:
                    expert_add_info_format(pinfo, service_item, &ei_devicenet_invalid_service,
                        "Invalid service code (0x%x) for Group 3 Message ID 6", service_rr & CIP_SC_MASK);
                    break;
                }
                break;
            case 0x1C0:
                expert_add_info_format(pinfo, msg_id_item, &ei_devicenet_invalid_msg_id,
                        "Invalid Group 3 Message ID (%d)", message_id);
                break;
            }

            switch(service_rr & CIP_SC_MASK)
            {
            case SC_OPEN_EXPLICIT_MESSAGE:
                /* XXX - Create conversation to track connections */
                if (service_rr & CIP_SC_RESPONSE_MASK)
                {
                    proto_tree_add_item(content_tree, hf_devicenet_open_exp_msg_reserved, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                    proto_tree_add_item(content_tree, hf_devicenet_open_exp_msg_actual_body_format, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                    offset++;
                    proto_tree_add_item(content_tree, hf_devicenet_open_exp_dest_message_id, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                    proto_tree_add_item(content_tree, hf_devicenet_open_exp_src_message_id, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                    offset++;
                    proto_tree_add_item(content_tree, hf_devicenet_connection_id, tvb, offset, 2, ENC_LITTLE_ENDIAN);
                }
                else
                {
                    proto_tree_add_item(content_tree, hf_devicenet_open_exp_msg_reserved, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                    proto_tree_add_item(content_tree, hf_devicenet_open_exp_msg_req_body_format, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                    offset++;
                    proto_tree_add_item(content_tree, hf_devicenet_open_exp_group_select, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                    proto_tree_add_item(content_tree, hf_devicenet_open_exp_src_message_id, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                }
                break;
            case SC_CLOSE_EXPLICIT_MESSAGE:
                /* XXX - Use conversation to track connections */
                if ((service_rr & CIP_SC_RESPONSE_MASK) == 0)
                {
                    proto_tree_add_item(content_tree, hf_devicenet_connection_id, tvb, offset, 2, ENC_LITTLE_ENDIAN);
                }
                break;
            default:
                if(service_rr & CIP_SC_MASK)
                {
                    if (data_length - offset > 0) {
                        proto_tree_add_item(devicenet_tree, hf_devicenet_data, tvb, offset, data_length - 2, ENC_NA);
                    }
                }
                else
                {
                    guint channel;

                    for (channel = 0; channel < num_devicenet_records_uat; channel++)
                    {
                        if (uat_devicenet_records[channel].mac_id == source_mac)
                        {
                            switch(uat_devicenet_records[channel].behavior)
                            {
                            case 0:
                                body_type_8_over_8_dissection(data_length, content_tree, tvb, pinfo, offset);
                                break;
                            case 1:
                                body_type_8_over_16_dissection(data_length, content_tree, tvb, pinfo, offset);
                                break;
                            case 2:
                                body_type_16_over_8_dissection(data_length, content_tree, tvb, pinfo, offset);
                                break;
                            case 3:
                                body_type_16_over_16_dissection(data_length, content_tree, tvb, pinfo, offset);
                                break;
                            default:
                                if (data_length - offset > 0) {
                                    proto_tree_add_item(content_tree, hf_devicenet_data, tvb, offset, data_length, ENC_NA);
                                }
                                break;
                            }
                        }
                    }

                    /* Don't have a behavior defined for this address, default to 8 over 8 */
                    if (channel >= num_devicenet_records_uat)
                    {
                        body_type_8_over_8_dissection(data_length, content_tree, tvb, pinfo, offset);
                    }
                }
                break;
            }
        }
    }
    /*Message group 4*/
    else if (can_info.id <= MESSAGE_GROUP_4_ID )
    {
        ti = proto_tree_add_uint(can_tree, hf_devicenet_grp_msg4_id, tvb, 0, 0, can_info.id);
        proto_item_set_generated(ti);

        message_id = can_info.id & MESSAGE_GROUP_4_MSG_MASK;
        col_set_str(pinfo->cinfo, COL_INFO, val_to_str_const(message_id, devicenet_grp_msg4_vals, "Reserved Group 4 Message"));

        switch(message_id)
        {
        case GRP4_COMM_FAULT_RESPONSE:
        case GRP4_COMM_FAULT_REQUEST:
            if(data_length == 2)
            {
                proto_tree_add_item(devicenet_tree, hf_devicenet_comm_fault_rsv, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                proto_tree_add_item(devicenet_tree, hf_devicenet_comm_fault_match, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                proto_tree_add_item(devicenet_tree, hf_devicenet_comm_fault_value, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                offset++;

                proto_tree_add_item(devicenet_tree, hf_devicenet_rr_bit, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                proto_tree_add_item(devicenet_tree, hf_devicenet_service_code, tvb, offset, 1, ENC_LITTLE_ENDIAN);

                if( tvb_get_guint8(tvb, offset) & CIP_SC_RESPONSE_MASK)
                {
                    col_append_str(pinfo->cinfo, COL_INFO, " - Response");
                }
                else
                {
                    col_append_str(pinfo->cinfo, COL_INFO, " - Request");
                }
            }
            else if(data_length == 8)
            {
                proto_tree_add_item(devicenet_tree, hf_devicenet_comm_fault_rsv, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                offset++;

                proto_tree_add_item(devicenet_tree, hf_devicenet_rr_bit, tvb, offset, 1, ENC_LITTLE_ENDIAN);
                proto_tree_add_item(devicenet_tree, hf_devicenet_service_code, tvb, offset, 1, ENC_LITTLE_ENDIAN);

                if( tvb_get_guint8(tvb, offset) & CIP_SC_RESPONSE_MASK)
                {
                    col_append_str(pinfo->cinfo, COL_INFO, " - Response");
                }
                else
                {
                    col_append_str(pinfo->cinfo, COL_INFO, " - Request");
                }
                offset++;

                proto_tree_add_item(devicenet_tree, hf_devicenet_vendor, tvb, offset, 2, ENC_LITTLE_ENDIAN);
                offset +=2;
                proto_tree_add_item(devicenet_tree, hf_devicenet_serial_number, tvb, offset, 4, ENC_LITTLE_ENDIAN);
            }
            break;
        case GRP4_OFFLINE_OWNER_REQUEST:
        case GRP4_OFFLINE_OWNER_RESPONSE:
            proto_tree_add_item(devicenet_tree, hf_devicenet_offline_ownership_reserved,  tvb, offset, 1, ENC_LITTLE_ENDIAN);
            proto_tree_add_item(devicenet_tree, hf_devicenet_offline_ownership_client_mac_id, tvb, offset, 1, ENC_LITTLE_ENDIAN);
            offset++;

            proto_tree_add_item(devicenet_tree, hf_devicenet_rr_bit, tvb, offset, 1, ENC_LITTLE_ENDIAN);

            if( tvb_get_guint8(tvb, offset) & CIP_SC_RESPONSE_MASK)
            {
                col_append_str(pinfo->cinfo, COL_INFO, " - Response");
            }
            else
            {
                col_append_str(pinfo->cinfo, COL_INFO, " - Request");
            }

            proto_tree_add_item(devicenet_tree, hf_devicenet_offline_ownership_allocate, tvb, offset, 1, ENC_LITTLE_ENDIAN);
            offset++;
            proto_tree_add_item(devicenet_tree, hf_devicenet_vendor, tvb, offset, 2, ENC_LITTLE_ENDIAN);
            offset +=2;
            proto_tree_add_item(devicenet_tree, hf_devicenet_serial_number, tvb, offset, 4, ENC_LITTLE_ENDIAN);
            break;
        }
    }
    /*Invalid CAN message*/
    else
    {
        col_add_fstr(pinfo->cinfo, COL_INFO, "Invalid CAN Message 0x%06X", can_info.id);
        expert_add_info_format(pinfo, can_id_item, &ei_devicenet_invalid_can_id,
                    "Invalid CAN Message 0x%04X", can_info.id);
    }

    return tvb_captured_length(tvb);
}

static int devicenet_addr_to_str(const address* addr, gchar *buf, int buf_len)
{
    const guint8 *addrdata = (const guint8 *)addr->data;

    guint32_to_str_buf(*addrdata, buf, buf_len);
    return (int)strlen(buf);
}

static int devicenet_addr_str_len(const address* addr _U_)
{
    return 11; /* Leaves required space (10 bytes) for uint_to_str_back() */
}

static int devicenet_addr_len(void)
{
    return 1;
}

void proto_register_devicenet(void)
{
    module_t *devicenet_module;
    expert_module_t*expert_devicenet;

    static hf_register_info hf[] = {
        { &hf_devicenet_can_id,
            {"CAN Identifier", "devicenet.can_id",
            FT_UINT16, BASE_HEX, NULL, DEVICENET_CANID_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_src_mac_id,
            { "Source MAC ID", "devicenet.src_mac_id",
            FT_UINT8, BASE_DEC, NULL, 0,
            NULL, HFILL }
        },
        { &hf_devicenet_grp1_slave_mac_id,
            { "Slave MAC ID", "devicenet.slave_mac_id",
            FT_UINT16, BASE_DEC, NULL, MESSAGE_GROUP_1_MAC_ID_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_grp2_slave_mac_id,
            { "Slave MAC ID", "devicenet.slave_mac_id",
            FT_UINT16, BASE_DEC, NULL, MESSAGE_GROUP_2_MAC_ID_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_allocators_mac_id,
            { "Allocator's MAC ID", "devicenet.allocators_mac_id",
            FT_UINT8, BASE_DEC, NULL, MESSAGE_EXPLICIT_MAC_ID_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_connection_id,
            { "Connection ID", "devicenet.connection_id",
            FT_UINT16, BASE_DEC, NULL, 0,
            NULL, HFILL }
        },
        { &hf_devicenet_data,
            { "Data", "devicenet.data",
            FT_BYTES, BASE_NONE, NULL, 0,
            NULL, HFILL }
        },
        { &hf_devicenet_abb_status,
            { "ABB Status", "devicenet.abb.status",
            FT_UINT16, BASE_HEX, NULL, 0,
            NULL, HFILL }
        },
        { &hf_devicenet_abb_timestamp,
            { "ABB Timestamp", "devicenet.abb.timestamp",
            FT_UINT32, BASE_DEC, NULL, 0,
            NULL, HFILL }
        },
        { &hf_devicenet_grp_msg1_id,
            { "Group 1 message ID", "devicenet.grp_msg1.id",
            FT_UINT16, BASE_DEC, NULL, MESSAGE_GROUP_1_MSG_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_grp_msg2_id,
            { "Group 2 message ID", "devicenet.grp_msg2.id",
            FT_UINT16, BASE_DEC, NULL, MESSAGE_GROUP_2_MSG_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_grp_msg2_frag,
            { "Frag", "devicenet.grp_msg2.frag",
            FT_BOOLEAN, 8, NULL, MESSAGE_GROUP_2_FRAG_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_grp_msg2_xid,
            { "XID", "devicenet.grp_msg2.xid",
            FT_BOOLEAN, 8, NULL, MESSAGE_GROUP_2_XID_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_grp_msg2_mac_id,
            { "MAC ID", "devicenet.grp_msg2.mac_id",
            FT_UINT8, BASE_DEC, NULL, MESSAGE_EXPLICIT_MAC_ID_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_grp_msg3_id,
            { "Group 3 message ID", "devicenet.grp_msg3.id",
            FT_UINT16, BASE_DEC, NULL, MESSAGE_GROUP_3_MSG_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_grp_msg3_dest_mac_id,
            { "Destination MAC ID", "devicenet.dest_mac_id",
            FT_UINT8, BASE_DEC, NULL, MESSAGE_EXPLICIT_MAC_ID_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_grp_msg3_frag,
            { "Frag", "devicenet.grp_msg3.frag",
            FT_BOOLEAN, 8, NULL, MESSAGE_GROUP_3_FRAG_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_grp_msg3_xid,
            { "XID", "devicenet.grp_msg3.xid",
            FT_BOOLEAN, 8, NULL, MESSAGE_GROUP_3_XID_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_grp_msg4_id,
            { "Group 4 message ID", "devicenet.grp_msg4.id",
            FT_UINT16, BASE_DEC, NULL, MESSAGE_GROUP_4_MSG_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_rr_bit,
            { "Request/Response", "devicenet.rr",
            FT_UINT8, BASE_DEC, VALS(cip_sc_rr), CIP_SC_RESPONSE_MASK,
            "Request or Response message", HFILL }
        },
        { &hf_devicenet_service_code,
            { "Service Code", "devicenet.service",
            FT_UINT8, BASE_DEC, VALS(devicenet_grp2_service_code_vals), CIP_SC_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_open_exp_src_message_id,
            { "Source Message ID", "devicenet.open_message.src_message_id",
            FT_UINT8, BASE_DEC, NULL, 0x0F,
            NULL, HFILL }
        },
        { &hf_devicenet_open_exp_dest_message_id,
            { "Destination Message ID", "devicenet.open_message.dest_message_id",
            FT_UINT8, BASE_DEC, NULL, 0xF0,
            NULL, HFILL }
        },
        { &hf_devicenet_open_exp_msg_reserved,
            { "Reserved", "devicenet.open_message.reserved",
            FT_UINT8, BASE_DEC, NULL, 0xF0,
            NULL, HFILL }
        },
        { &hf_devicenet_open_exp_msg_req_body_format,
            { "Requested Message Body Format", "devicenet.open_message.req_body_format",
            FT_UINT8, BASE_DEC, VALS(devicenet_message_body_format_vals), 0x0F,
            NULL, HFILL }
        },
        { &hf_devicenet_open_exp_msg_actual_body_format,
            { "Actual Message Body Format", "devicenet.open_message.actual_body_format",
            FT_UINT8, BASE_DEC, VALS(devicenet_message_body_format_vals), 0x0F,
            NULL, HFILL }
        },
        { &hf_devicenet_open_exp_group_select,
            { "Group Select", "devicenet.open_message.group_select",
            FT_UINT8, BASE_DEC, VALS(devicenet_group_select_vals), 0xF0,
            NULL, HFILL }
        },
        { &hf_devicenet_dup_mac_id,
            { "Duplicate MAC ID check", "devicenet.dup_mac_id.dup_mac_id",
            FT_UINT8, BASE_HEX, NULL, 0xff,
            NULL, HFILL }
        },
        { &hf_devicenet_dup_mac_id_physical_port_number,
            { "Physical port number", "devicenet.dup_mac_id.physical_port_number",
            FT_UINT8, BASE_DEC, NULL, 0x7F,
            "Duplicate MAC ID check message physical port number", HFILL }
        },
        { &hf_devicenet_dup_mac_id_vendor,
            { "Vendor ID", "devicenet.dup_mac_id.vendor",
            FT_UINT16, BASE_HEX|BASE_EXT_STRING, &cip_vendor_vals_ext, 0,
            NULL, HFILL }
        },
        { &hf_devicenet_group2_only_unconnected_explicit_request_service,
            { "Service", "devicenet.unconnected_port_request.service",
            FT_UINT8, BASE_HEX, VALS(devicenet_grp2_service_code_vals), CIP_SC_MASK,
            NULL, HFILL }
        },
        { &hf_devicenet_group2_explicit_request_class_id,
            { "Class ID", "devicenet.explicit_request.class_id",
            FT_UINT8, BASE_HEX, NULL, 0,
            NULL, HFILL }
        },
        { &hf_devicenet_group2_explicit_request_instance_id,
            { "Instance ID", "devicenet.explicit_request.instance_id",
            FT_UINT8, BASE_HEX, NULL, 0,
            NULL, HFILL }
        },
        { &hf_devicenet_group2_explicit_request_allocation_choice,
            { "Allocation Choice", "devicenet.explicit_requeset.allocation_choice",
            FT_UINT8, BASE_HEX, NULL, 0,
            NULL, HFILL }
        },

        // ODVA Volume 1, Release 2.0, 4-2.1 Explicit Messaging Message Header
        { &hf_devicenet_explicit_header,
            { "Explicit Messaging Header", "devicenet.explicit.header",
            FT_UINT8, BASE_HEX, NULL, 0xff,
            NULL, HFILL }
        },

        // ODVA Volume 1, Release 2.0, Chapter 4-2.1 Explicit Messaging Message Header
        { &hf_devicenet_explicit_header_frag,
            { "Fragmentation", "devicenet.explicit.frag",
            FT_BOOLEAN, 8, NULL, (1 << 7),
            NULL, HFILL}
        },
        { &hf_devicenet_explicit_header_xid,
            { "Transaction ID", "devicenet.explicit.xid",
            FT_BOOLEAN, 8, NULL, (1 << 6),
            NULL, HFILL }
        },
        { &hf_devicenet_explicit_header_slave_mac_id,
            { "Slave MAC ID", "devicenet.explicit.slave_mac_id",
            FT_UINT8, BASE_DEC, NULL, MESSAGE_EXPLICIT_MAC_ID_MASK,
            NULL, HFILL }
        },

        { &hf_devicenet_fragmentation_protocol,
            { "Fragmentation", "devicenet.explicit.fragmentation",
            FT_UINT8, BASE_HEX, NULL, 0xff,
            NULL, HFILL }
        },

        { &hf_devicenet_service_field,
            { "Service Field", "devicenet.explicit.service",
            FT_UINT8, BASE_HEX, NULL, 0xff,
            NULL, HFILL}
        },
        { &hf_devicenet_service_field_rr,
            { "Request/Response", "devicenet.explicit.rr",
            FT_BOOLEAN, 8, TFS(&service_field_rr), CIP_SC_RESPONSE_MASK,
            "Request or Response message", HFILL }
        },
        { &hf_devicenet_service_field_service_code,
            { "Service Code", "devicenet.explicit.service",
            FT_UINT8, BASE_DEC, VALS(devicenet_grp2_service_code_vals), CIP_SC_MASK,
            NULL, HFILL }
        },


        // ODVA Volume 1 5-5.4.2 Table 5.18. Allocation Choice Byte Contents
        { &hf_devicenet_allocation_choice,
            { "Allocation Choice", "devicenet.allocation_choice",
            FT_UINT8, BASE_HEX, NULL, 0xff,
            NULL, HFILL  }
        },

        { &hf_devicenet_allocation_choice_explicit_message,
            { "Explicit Message", "devicenet.allocation_choice.explicit_message",
            FT_BOOLEAN, 8, NULL, (1 << 0),
            NULL, HFILL }
        },
        { &hf_devicenet_allocation_choice_polled,
            { "Polled", "devicenet.allocation_choice.polled",
            FT_BOOLEAN, 8, NULL, (1 << 1),
            NULL, HFILL }
        },
        { &hf_devicenet_allocation_choice_bit_strobed,
            { "Bit Strobed", "devicenet.allocation_choice.bit_strobed",
            FT_BOOLEAN, 8, NULL, (1 << 2),
            NULL, HFILL }
        },
        { &hf_devicenet_allocation_choice_reserved3,
            { "Reserved", "devicenet.allocation_choice.reserved3",
            FT_BOOLEAN, 8, NULL, (1 << 3),
            NULL, HFILL }
        },
        { &hf_devicenet_allocation_choice_change_of_state,
            { "Change of State", "devicenet.allocation_choice.change_of_state",
            FT_BOOLEAN, 8, NULL, (1 << 4),
            NULL, HFILL }
        },
        { &hf_devicenet_allocation_choice_cyclic,
            { "Cyclic", "devicenet.allocation_choice.cyclic",
            FT_BOOLEAN, 8, NULL, (1 << 5),
            NULL, HFILL }
        },
        { &hf_devicenet_allocation_choice_acknowledge_suppression,
            { "Acknowledge Suppression", "devicenet.allocation_choice.acknowledge_suppression",
            FT_BOOLEAN, 8, NULL, (1 << 6),
            NULL, HFILL }
        },
        { &hf_devicenet_allocation_choice_reserved7,
            { "Reserved", "devicenet.allocation_choice.reserved7",
            FT_BOOLEAN, 8, NULL, (1 << 7),
            NULL, HFILL }
        },


        { &hf_devicenet_dup_mac_id_serial_number,
            { "Serial Number", "devicenet.dup_mac_id.serial_number",
            FT_UINT32, BASE_HEX, NULL, 0x00,
            NULL, HFILL }
        },
        { &hf_devicenet_vendor,
            { "Vendor ID", "devicenet.vendor",
            FT_UINT16, BASE_HEX|BASE_EXT_STRING, &cip_vendor_vals_ext, 0,
            NULL, HFILL }
        },
        { &hf_devicenet_serial_number,
            { "Serial Number", "devicenet.serial_number",
            FT_UINT32, BASE_HEX, NULL, 0x00,
            NULL, HFILL }
        },
        { &hf_devicenet_instance8,
            { "Instance", "devicenet.instance",
            FT_UINT8, BASE_HEX, NULL, 0x00,
            NULL, HFILL }
        },
        { &hf_devicenet_instance16,
            { "Instance", "devicenet.instance",
            FT_UINT16, BASE_HEX, NULL, 0x00,
            NULL, HFILL }
        },
        { &hf_devicenet_attribute,
            { "Attribute", "devicenet.attribute",
            FT_UINT8, BASE_HEX, NULL, 0x00,
            NULL, HFILL }
        },
        { &hf_devicenet_fragment_type,
            { "Fragment Type", "devicenet.fragment_type",
            FT_UINT8, BASE_HEX, VALS(devicenet_fragmented_message_type_vals), 0xC0,
            NULL, HFILL }
        },
        { &hf_devicenet_grp2_fragment_type,
            { "Fragment Type", "devicenet.fragment_type",
            FT_UINT8, BASE_HEX, VALS(devicenet_grp2_fragmented_message_type_vals), 0xC0,
            NULL, HFILL }
        },
        { &hf_devicenet_fragment_count,
            { "Fragment Count", "devicenet.fragment_count",
            FT_UINT8, BASE_HEX_DEC, NULL, 0x3F,
            NULL, HFILL }
        },
        { &hf_devicenet_class8,
            { "Class",  "devicenet.class",
            FT_UINT8, BASE_HEX|BASE_EXT_STRING, &cip_class_names_vals_ext, 0,
            NULL, HFILL }
        },
        { &hf_devicenet_class16,
            { "Class",  "devicenet.class",
            FT_UINT16, BASE_HEX|BASE_EXT_STRING, &cip_class_names_vals_ext, 0,
            NULL, HFILL }
        },
        { &hf_devicenet_comm_fault_rsv,
            { "Reserved",  "devicenet.comm_fault.reserved",
            FT_UINT8, BASE_HEX, NULL, 0x80,
            NULL, HFILL }
        },
        { &hf_devicenet_comm_fault_match,
            { "Match", "devicenet.comm_fault.match",
            FT_UINT8, BASE_HEX, NULL, 0x40,
            NULL, HFILL }
        },
        {&hf_devicenet_comm_fault_value,
            { "Value", "devicenet.comm_fault.value",
            FT_UINT8, BASE_HEX, NULL, 0x3F,
            "Comm Fault Value", HFILL }
        },
        {&hf_devicenet_offline_ownership_reserved,
            { "Reserved", "devicenet.offline_ownership.reserved",
            FT_UINT8, BASE_HEX, NULL, 0xC0,
            "Offline ownership Response Message Reserved", HFILL }
        },
        {&hf_devicenet_offline_ownership_client_mac_id,
            { "Client MAC ID", "devicenet.offline_ownership.client_mac_id",
            FT_UINT8, BASE_HEX, NULL, MESSAGE_GROUP_4_MSG_MASK,
            "Offline ownership message client MAC ID", HFILL }
        },
        {&hf_devicenet_offline_ownership_allocate,
            { "Allocate", "devicenet.offline_ownership.allocate",
            FT_UINT8, BASE_HEX, NULL, CIP_SC_MASK,
            "Offline ownership response message allocate", HFILL }
        },
    };

    static gint *ett[] = {
        &ett_devicenet,
        &ett_devicenet_can,
        &ett_devicenet_contents,
        &ett_devicenet_explicit_header,
        &ett_devicenet_fragmentation,
        &ett_devicenet_service_field,
        &ett_devicenet_dup_mac_id_fields,
        &ett_devicenet_allocation_choice,
        &ett_devicenet_8_8,
        &ett_devicenet_8_16,
        &ett_devicenet_16_8,
        &ett_devicenet_16_16
    };

    static ei_register_info ei[] = {
        { &ei_devicenet_invalid_service, { "devicenet.invalid_service", PI_PROTOCOL, PI_WARN, "Invalid service", EXPFILL }},
        { &ei_devicenet_invalid_can_id, { "devicenet.invalid_can_id", PI_PROTOCOL, PI_WARN, "Invalid CAN ID", EXPFILL }},
        { &ei_devicenet_invalid_msg_id, { "devicenet.invalid_msg_id", PI_PROTOCOL, PI_WARN, "Invalid Message ID", EXPFILL }},
        { &ei_devicenet_frag_not_supported, { "devicenet.frag_not_supported", PI_UNDECODED, PI_WARN, "Fragmentation not currently supported", EXPFILL }},
    };

    static uat_field_t devicenet_uat_flds[] = {
        UAT_FLD_DEC(uat_devicenet_records, mac_id, "Option number", "Custom Option Number"),
        UAT_FLD_VS(uat_devicenet_records, behavior, "Option type", devicenet_message_body_format_vals, "Option datatype"),
        UAT_END_FIELDS
    };

    proto_devicenet = proto_register_protocol("DeviceNet Protocol", "DeviceNet", "devicenet");

    proto_register_field_array(proto_devicenet, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));
    expert_devicenet = expert_register_protocol(proto_devicenet);
    expert_register_field_array(expert_devicenet, ei, array_length(ei));

    devicenet_address_type = address_type_dissector_register("AT_DEVICENET", "DeviceNet Address", devicenet_addr_to_str, devicenet_addr_str_len, NULL, NULL, devicenet_addr_len, NULL, NULL);

    devicenet_module = prefs_register_protocol(proto_devicenet, NULL);

    devicenet_uat = uat_new("Node bodytypes",
                            sizeof(uat_devicenet_record_t), /* record size           */
                            "devicenet_bodytypes",          /* filename              */
                            TRUE,                           /* from_profile          */
                            &uat_devicenet_records,         /* data_ptr              */
                            &num_devicenet_records_uat,     /* numitems_ptr          */
                            UAT_AFFECTS_DISSECTION,         /* affects dissection of packets, but not set of named fields */
                            NULL,                           /* help                  */
                            NULL,   /* copy callback         */
                            uat_devicenet_record_update_cb, /* update callback       */
                            NULL,   /* free callback         */
                            NULL,    /* post update callback  */
                            NULL,   /* reset callback */
                            devicenet_uat_flds);    /* UAT field definitions */

    prefs_register_uat_preference(devicenet_module,
                                      "bodytype_table",
                                      "Node bodytypes",
                                      "Node bodytypes",
                                      devicenet_uat);
}

void
proto_reg_handoff_devicenet(void)
{
    dissector_handle_t devicenet_handle;

    devicenet_handle = create_dissector_handle( dissect_devicenet, proto_devicenet );
    dissector_add_for_decode_as("can.subdissector", devicenet_handle );
}

/*
 * Editor modelines  -  https://www.wireshark.org/tools/modelines.html
 *
 * Local variables:
 * c-basic-offset: 4
 * tab-width: 8
 * indent-tabs-mode: nil
 * End:
 *
 * vi: set shiftwidth=4 tabstop=8 expandtab:
 * :indentSize=4:tabSize=8:noTabs=true:
 */
